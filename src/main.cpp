#include <ctime>
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include <sys/time.h>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui_c.h>

#include "tracker.h"
#include "detector.h"
#include "dataReader.h"
#include "multiTrackAssociation.h"
#include "parameter.h"

#include "counter.h"

using namespace cv;
using namespace std;

static string _sequence_path_;
static string _detection_xml_file_;

//configuration parameters via config.txt file
int MAX_TRACKER_NUM;
int MAX_TEMPLATE_SIZE;
int EXPERT_THRESH;
double BODYSIZE_TO_DETECTION_RATIO;
double TRACKING_TO_BODYSIZE_RATIO;
int FRAME_RATE;
double TIME_WINDOW_SIZE;
double HOG_DETECT_FRAME_RATIO;
double VIDEO_SCALE;
int DRAW_DEBUG_DETECTIONS;
int DRAW_DETECTIONS;
int WRITE_XML;

//virtual detector line parameters
Line vl;
Point p1_vld, p2_vld;
double delta = 50.5;

//MouseCallBackFunc variables in order to set virtual detector line using mouse and graphical interface
//bool pressed = false;
//bool ok = false;
//int testRes = 0;
//Point test;

//size of the video frame
Size frameSize;

PeopleCounter pc;

// fps counter variables
time_t start, end;
int counter = 0;
double sec;
double fps;
timeval curTime;

XMLBBoxWriter XMLresultWriter("pippo.xml");

/*void MouseCallBackFunc(int event, int x, int y, int flags, void* userdata) {
 if (event == EVENT_LBUTTONDOWN) {
 cout << "Left button of the mouse is clicked - position (" << x << ", "
 << y << ")" << endl;
 if (!pressed) {
 p1_vld = cv::Point(x, y);
 pressed = !pressed;
 ok = false;
 } else {
 p2_vld = cv::Point(x, y);
 pressed = !pressed;
 ok = true;
 }
 //compute virtual crossing lines
 if (ok) {
 vl.ComputeVirtualLine(p1_vld, p2_vld, frameSize);
 }
 } else if (event == EVENT_RBUTTONDOWN) {
 cout << "Right button of the mouse is clicked - position (" << x << ", "
 << y << ")" << endl;
 test.x = x;
 test.y = y;
 testRes = -5;
 testRes = vl.FindSide(vl.GetVP1().x, vl.GetVP1().y, vl.GetVP2().x,
 vl.GetVP2().y, x, y);
 }
 //    else if  ( event == EVENT_MBUTTONDOWN )
 //    {
 //        cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
 //    }
 //    else if ( event == EVENT_MOUSEMOVE )
 //    {
 //        cout << "Mouse move over the window - position (" << x << ", " << y << ")" << endl;
 //    }
 }*/

void read_config() {
	ifstream conf_file("config.txt");

	if (!conf_file.is_open()) {
		cerr << "fail to load config.txt." << endl;
		exit(1);
	}

	string line;
	while (conf_file.good()) {
		getline(conf_file, line);
		istringstream line_s(line);
		string field;
		line_s >> field;
		if (field.compare("MAX_TRACKER_NUM:") == 0)
			line_s >> MAX_TRACKER_NUM;
		else if (field.compare("FRAME_RATE:") == 0)
			line_s >> FRAME_RATE;
		else if (field.compare("TIME_WINDOW_SIZE:") == 0)
			line_s >> TIME_WINDOW_SIZE;
		else if (field.compare("HOG_DETECT_FRAME_RATIO:") == 0)
			line_s >> HOG_DETECT_FRAME_RATIO;
		else if (field.compare("MAX_TEMPLATE_SIZE:") == 0)
			line_s >> MAX_TEMPLATE_SIZE;
		else if (field.compare("EXPERT_THRESH:") == 0)
			line_s >> EXPERT_THRESH;
		else if (field.compare("BODYSIZE_TO_DETECTION_RATIO:") == 0)
			line_s >> BODYSIZE_TO_DETECTION_RATIO;
		else if (field.compare("TRACKING_TO_BODYSIZE_RATIO:") == 0)
			line_s >> TRACKING_TO_BODYSIZE_RATIO;
		else if (field.compare("VIDEO_SCALE:") == 0)
			line_s >> VIDEO_SCALE;
		else if (field.compare("DRAW_DETECTIONS:") == 0)
			line_s >> DRAW_DETECTIONS;
		else if (field.compare("DRAW_DEBUG_DETECTIONS:") == 0)
			line_s >> DRAW_DEBUG_DETECTIONS;
		else if (field.compare("WRITE_XML:") == 0)
			line_s >> WRITE_XML;
		else if (field.compare("P1X:") == 0)
			line_s >> p1_vld.x;
		else if (field.compare("P1Y:") == 0)
			line_s >> p1_vld.y;
		else if (field.compare("P2X:") == 0)
			line_s >> p2_vld.x;
		else if (field.compare("P2Y:") == 0)
			line_s >> p2_vld.y;
	}
	conf_file.close();
}

void multiTrack(int readerType, int detectorType) {

	if (DRAW_DETECTIONS)
		namedWindow("multiTrack", CV_WINDOW_AUTOSIZE);

	//set the callback function for any mouse event
	//cv::setMouseCallback("multiTrack", MouseCallBackFunc, NULL);
	//this function is only used in debug mode to set virtual detection line using mouse
	//else we have to set the virtual detection line coordinates via confi.txt file

	SeqReader* reader;
	Mat frame;
	switch (readerType) {
	case IMAGE:
		reader = new ImageDataReader(_sequence_path_);
		break;
	case VIDEO:
		reader = new VideoReader(_sequence_path_);
		break;
	default:
		cerr << "no such reader type!" << endl;
		return;
	}

	//read video frame
	reader->readImg(frame);

	//video frame resizing
	Mat frame_res;
	frame.copyTo(frame_res);
	resize(frame, frame_res, Size(0, 0), VIDEO_SCALE, VIDEO_SCALE, INTER_CUBIC);
	frameSize.height = frame_res.rows;
	frameSize.width = frame_res.cols;

	if (frame_res.data == NULL) {
		cerr << "fail to open pictures!" << endl;
		return;
	}

	//compute virtual detector line parameters
	//p1_vld = cv::Point(90, 186);
	//p2_vld = cv::Point(519, 209);
	vl.ComputeVirtualLine(p1_vld, p2_vld, frameSize);

	Detector* detector;
	switch (detectorType) {
	case HOG:
		detector = new HogDetector();
		break;
	case XML:
		detector = new XMLDetector(_detection_xml_file_.c_str());
		break;
	default:
		detector = new HogDetector();
		break;
	}

	TrakerManager mTrack(detector, frame_res, EXPERT_THRESH);

	for (int frameCount = 0; frame_res.data != NULL; frameCount++) {

		if (frameCount == 0)
			time(&start);

		mTrack.doWork(frame_res);

		time(&end);
		sec = difftime(end, start);
		fps = frameCount / sec;

		//cout << "fps: " << fps << endl;

		char buffer[255];
		string ss;
		sprintf(buffer, "fps: %3.2f in: %05d - out: %05d - total (in+out): %05d", fps, pc.GetPeopleIn(),
				pc.GetPeopleOut(), pc.GetPeopleTotal());
		ss = buffer;
		cout << ss << std::endl;
		//********************************************************************************************************************
		list<EnsembleTracker*> risultati = mTrack.getEnsembleTracker();

		pc.UpdateCounter(risultati, vl, frameSize, frame_res);

		// draw results

		for (list<EnsembleTracker*>::iterator i = risultati.begin();
				i != risultati.end(); i++) {

			Rect win = (*i)->getResultHistory().back();
			Point center;
			center.x = scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).x
					+ scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).width / 2;
			center.y = scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).y
					+ scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).height / 2;

			double distS, distL;
			vl.DistanceFromLine(center.x, center.y, vl.GetVP1().x,
					vl.GetVP1().y, vl.GetVP2().x, vl.GetVP2().y, distS, distL);

			if (distS <= 50) {
				(*i)->drawResult(frame_res, 1 / TRACKING_TO_BODYSIZE_RATIO);

				Point tx(win.x, win.y - 1);
				char buff[10];
				sprintf(buff, "%d", (*i)->getID());
				string s = buff;
				putText(frame_res, s, tx, FONT_HERSHEY_PLAIN, 1.5,
						COLOR((*i)->getID()), 2);

				/* boh
				 //			int xxx = (*i)->getResultHistory().size();
				 //			if (xxx < 10)
				 //				cout << "pipppppppppppppppo" << endl;
				 //			Rect win_previus =
				 //					(*i)->getResultHistory()[(*i)->getResultHistory().size()
				 //							- 10];
				 //			Point center_previus;
				 //			center_previus.x =
				 //					scaleWin(win_previus, 1 / TRACKING_TO_BODYSIZE_RATIO).x
				 //							+ scaleWin(win_previus,
				 //									1 / TRACKING_TO_BODYSIZE_RATIO).width / 2;
				 //			center_previus.y =
				 //					scaleWin(win_previus, 1 / TRACKING_TO_BODYSIZE_RATIO).y
				 //							+ scaleWin(win_previus,
				 //									1 / TRACKING_TO_BODYSIZE_RATIO).height / 2;
				 //			cv::circle(frame_res, center_previus, 2, cv::Scalar(255, 255, 255),
				 //					2);
				 }
				 */

			}

			//********************************************************************************************************************

			char buff[255];
			string s;
			sprintf(buff, "in: %05d - out: %05d ", pc.GetPeopleIn(),
					pc.GetPeopleOut());
			s = buff;
			putText(frame_res, s, cv::Point(50, frameSize.height - 80),
					FONT_HERSHEY_PLAIN, 1.7, cv::Scalar(255, 255, 255), 2);

			sprintf(buff, "total (in+out): %05d", pc.GetPeopleTotal());
			s = buff;
			putText(frame_res, s, cv::Point(50, frameSize.height - 50),
					FONT_HERSHEY_PLAIN, 1.7, cv::Scalar(255, 255, 255), 2);

			if (DRAW_DETECTIONS == 1) {
				cv::line(frame_res, vl.GetVP1(), vl.GetVP2(),
						cv::Scalar(0, 0, 255), 3);
				/*
				 // uncomment these lines if use wit mousecallback funciotn
				 pressed=false;
				 if (!pressed) {
				 //cv::line(frame_res, p1_vld, p2_vld, cv::Scalar(0, 255, 0), 5);

				 cv::line(frame_res, vl.GetVP1(), vl.GetVP2(),
				 cv::Scalar(0, 0, 255), 3);

				 //draw side_test_point result
				 if (testRes == 1)
				 cv::circle(frame_res, test, 3, cv::Scalar(0, 255, 0), 3);
				 else if (testRes == -1)
				 cv::circle(frame_res, test, 3, cv::Scalar(0, 0, 255), 3);
				 else if (testRes == 0)
				 cv::circle(frame_res, test, 3, cv::Scalar(255, 255, 255),
				 3);
				 else
				 cv::circle(frame_res, test, 4, cv::Scalar(0, 0, 0), 4);
				 }*/
				imshow("multiTrack", frame_res);
			}
		}
		reader->readImg(frame);
		frame.copyTo(frame_res);
		resize(frame, frame_res, Size(0, 0), VIDEO_SCALE, VIDEO_SCALE,
				INTER_CUBIC);

		gettimeofday(&curTime, NULL);
		int milli =curTime.tv_usec / 1000;
		time_t rawtime;
		struct tm * timeinfo;
		time(&rawtime);
		timeinfo=localtime(&rawtime);
		char buff[80];
		strftime(buff, 80, "%Y-%m-%d %H:%M:%S", timeinfo);
		char currentime[84]="";
		sprintf(currentime, "%s:%d", buff,milli);
		printf("currenttime: %s \n", currentime);
		XMLresultWriter.putNextFrameResult2(currentime, pc.GetPeopleIn(),pc.GetPeopleOut(), pc.GetPeopleTotal());

		char c = waitKey(1);
//		if (c == 'q')
//			break;
//		else if (c == 'p') {
//			cvWaitKey(0);
//		} else if (c != -1) {
//			mTrack.setKey(c);
//		}
	}

	delete reader;
	delete detector;
}

void help() {
	cout
			<< "usage: \n\n"
					"1.\n"
					"Hierarchy_Ensemble <sequence_path> <is_image>\n"
					"(by default, it uses hog detector in opencv to detect pedestrians)\n\n"

					"2.\n"
					"Hierarchy_Ensemble <sequence_path> <is_image> <detection_xml_file_path>\n"
					"(it uses detection stored in the specified xml file. You may rescale the detection bounding box "
					"by tuning parameters in the \"he_config.txt\")\n\n"

					"<is_image>: \'1\' for image format data. \'0\' for video format data.\n";
	getchar();
}

int main(int argc, char** argv) {
	if (argc != 3 && argc != 4) {
		help();
		exit(1);
	}

	read_config();

	_sequence_path_ = string(argv[1]);
	int seq_format;

	if (atof(argv[2]) == 1.0)
		seq_format = IMAGE;
	else
		seq_format = VIDEO;

	if (argc > 3) {
		_detection_xml_file_ = string(argv[3]);
		multiTrack(seq_format, XML);
	} else
		multiTrack(seq_format, HOG);

	return 0;
}
