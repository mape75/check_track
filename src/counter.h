/*
 * counter.h
 *
 *  Created on: 15/gen/2015
 *      Author: mape
 */

#ifndef COUNTER_H_
#define COUNTER_H_

#include <list>
#include "tracker.h"
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class Line {
public:
	Line();
	~Line();
	Line(Point p1, Point p2, Size frameSize);
	void ComputeVirtualLine(Point p1, Point p2, Size frameSize);
	void ComputeVirtualLine(Line vl, double offset, Size frameSize);
	void SetP1(Point p1);
	void SetP2(Point p2);
	Point GetP1();
	Point GetP2();
	void SetVP1(Point vp1);
	void SetVP2(Point vp2);
	Point GetVP1();
	Point GetVP2();
	double Get_m();
	double Get_b();
	int FindSide(double ax, double ay, double bx, double by, double cx,
			double cy);
	void DistanceFromLine(double cx, double cy, double ax, double ay,
			double bx, double by, double &distanceSegment, double &distanceLine);
private:
	Point _p1, _p2;
	Point _vp1, _vp2;
	double _m, _b;
};

class PeopleCounter {
public:
	PeopleCounter();
	~PeopleCounter();
	void UpdateCounter(list<EnsembleTracker*> trackResult, Line vl, Size frameSize, cv::Mat& frame);
	int GetPeopleIn();
	int GetPeopleOut();
	int GetPeopleTotal();
private:
	int _in;
	int _out;
	long int _total;
};

#endif /* COUNTER_H_ */
