/*
 * counter.cpp
 *
 *  Created on: 15/gen/2015
 *      Author: mape
 */

#include "counter.h"
#include "util.h"

using namespace cv;
using namespace std;

//Default constructor
Line::Line() :
		_m(0), _b(0) {
	//left blank intentionally
}
//Point1_Object & Point2_Object were suppose to get
//Passed into here
Line::Line(Point p1, Point p2, Size frameSize) {

	_p1 = p1;
	_p2 = p2;

	//y-y1=(y2-y1)/(x2-x1)*(x-x1)=m*(x-x1)
	//y=y1+m*(x-x1);

	//derived from slope(m) intercept(b) form
	_m = (double) (p2.y - p1.y) / (p2.x - p1.x);
	_b = (double) p1.y - (double) (p1.x * (p2.y - p1.y)) / (p2.x - p1.x);

	_vp1.x = 0;
	_vp1.y = _p1.y + _m * (_vp1.x - _p1.x);

	_vp2.x = frameSize.width;
	_vp2.y = _p2.y + _m * (_vp2.x - _p2.x);

}

Line::~Line() {
}

void Line::ComputeVirtualLine(Point p1, Point p2, Size frameSize) {

	_p1 = p1;
	_p2 = p2;

	//y-y1=(y2-y1)/(x2-x1)*(x-x1)=m*(x-x1)
	//y=y1+m*(x-x1);

	//derived from slope(m) intercept(b) form
	_m = (double) (p2.y - p1.y) / (p2.x - p1.x);
	_b = (double) p1.y - (double) (p1.x * (p2.y - p1.y)) / (p2.x - p1.x);

	_vp1.x = 0;
	_vp1.y = _p1.y + _m * (_vp1.x - _p1.x);

	_vp2.x = frameSize.width;
	_vp2.y = _p2.y + _m * (_vp2.x - _p2.x);

}

void Line::ComputeVirtualLine(Line vl, double offset, Size frameSize) {

	_vp1.x = vl.GetVP1().x;
	_vp1.y = vl.Get_b() + offset;
	//_vp1.x=vl.GetVP1().x;
	//_vp1.y=vl.GetVP1().y-offset;

	_vp2.x = vl.GetVP2().x;
	_vp2.y = vl.Get_m() * frameSize.width + vl.Get_b() + offset;
	//_vp2.x=vl.GetVP2().x;
	//_vp2.y=vl.GetVP2().y-offset;

}

/*
 * @return integer code for which side of the line ab c is on.  1 means
 * left turn, -1 means right turn.  Returns
 * 0 if all three are on a line
 */
int Line::FindSide(double ax, double ay, double bx, double by, double cx,
		double cy) {

	double s = (bx - ax) * (cy - ay) - (by - ay) * (cx - ax);
	if (s > 0.0f)
		return 1;  // one side of the line formed by a,b vector (..left of line)
	else if (s < 0.0f)
		return -1; // other side of the line formed by a,b vector (..right of line)
	else
		return 0; // on the line

//	if (fabs(bx - ax) < 0.001) { // vertical line
//		if (cx < bx) {
//			return by > ay ? 1 : -1;
//		}
//		if (cx > bx) {
//			return by > ay ? -1 : 1;
//		}
//		return 0;
//	}
//	if (fabs(by - ay) < 0.001) { // horizontal line
//		if (cy < by) {
//			return bx > ax ? -1 : 1;
//		}
//		if (cy > by) {
//			return bx > ax ? 1 : -1;
//		}
//		return 0;
//	}
//	double slope = (by - ay) / (bx - ax);
//	double yIntercept = ay - ax * slope;
//	double cSolution = (slope * cx) + yIntercept;
//	if (slope != 0) {
//		if (cy > cSolution) {
//			return bx > ax ? 1 : -1;
//		}
//		if (cy < cSolution) {
//			return bx > ax ? -1 : 1;
//		}
//		return 0;
//	}
//	return 0;
}

//ACCESSORS
double Line::Get_m() {
	return _m;
}
double Line::Get_b() {
	return _b;
}

void Line::SetP1(Point p1) {
	_p1 = p1;
}

void Line::SetP2(Point p2) {
	_p2 = p2;
}

Point Line::GetP1() {
	return _p1;
}

Point Line::GetP2() {
	return _p2;
}

Point Line::GetVP1() {
	return _vp1;
}

Point Line::GetVP2() {
	return _vp2;
}

void Line::DistanceFromLine(double cx, double cy, double ax, double ay,
		double bx, double by, double &distanceSegment, double &distanceLine) {

	//
	// find the distance from the point (cx,cy) to the line
	// determined by the points (ax,ay) and (bx,by)
	//
	// distanceSegment = distance from the point to the line segment
	// distanceLine = distance from the point to the line (assuming
	//                                        infinite extent in both directions
	//

	/*

	 Subject 1.02: How do I find the distance from a point to a line?


	 Let the point be C (Cx,Cy) and the line be AB (Ax,Ay) to (Bx,By).
	 Let P be the point of perpendicular projection of C on AB.  The parameter
	 r, which indicates P's position along AB, is computed by the dot product
	 of AC and AB divided by the square of the length of AB:

	 (1)    AC dot AB
	 r = ---------
	 ||AB||^2

	 r has the following meaning:

	 r=0      P = A
	 r=1      P = B
	 r<0      P is on the backward extension of AB
	 r>1      P is on the forward extension of AB
	 0<r<1    P is interior to AB

	 The length of a line segment in d dimensions, AB is computed by:

	 L = sqrt( (Bx-Ax)^2 + (By-Ay)^2 + ... + (Bd-Ad)^2)

	 so in 2D:

	 L = sqrt( (Bx-Ax)^2 + (By-Ay)^2 )

	 and the dot product of two vectors in d dimensions, U dot V is computed:

	 D = (Ux * Vx) + (Uy * Vy) + ... + (Ud * Vd)

	 so in 2D:

	 D = (Ux * Vx) + (Uy * Vy)

	 So (1) expands to:

	 (Cx-Ax)(Bx-Ax) + (Cy-Ay)(By-Ay)
	 r = -------------------------------
	 L^2

	 The point P can then be found:

	 Px = Ax + r(Bx-Ax)
	 Py = Ay + r(By-Ay)

	 And the distance from A to P = r*L.

	 Use another parameter s to indicate the location along PC, with the
	 following meaning:
	 s<0      C is left of AB
	 s>0      C is right of AB
	 s=0      C is on AB

	 Compute s as follows:

	 (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
	 s = -----------------------------
	 L^2


	 Then the distance from C to P = |s|*L.

	 */

	double r_numerator = (cx - ax) * (bx - ax) + (cy - ay) * (by - ay);
	double r_denomenator = (bx - ax) * (bx - ax) + (by - ay) * (by - ay);
	double r = r_numerator / r_denomenator;
//
	double px = ax + r * (bx - ax);
	double py = ay + r * (by - ay);
//
	double s = ((ay - cy) * (bx - ax) - (ax - cx) * (by - ay)) / r_denomenator;

	distanceLine = fabs(s) * sqrt(r_denomenator);

//
// (xx,yy) is the point on the lineSegment closest to (cx,cy)
//
	double xx = px;
	double yy = py;

	if ((r >= 0) && (r <= 1)) {
		distanceSegment = distanceLine;
	} else {

		double dist1 = (cx - ax) * (cx - ax) + (cy - ay) * (cy - ay);
		double dist2 = (cx - bx) * (cx - bx) + (cy - by) * (cy - by);
		if (dist1 < dist2) {
			xx = ax;
			yy = ay;
			distanceSegment = sqrt(dist1);
		} else {
			xx = bx;
			yy = by;
			distanceSegment = sqrt(dist2);
		}

	}

	return;
}

//Default constructor
PeopleCounter::PeopleCounter() :
		_in(0), _out(0) {
	//left blank intentionally
}

PeopleCounter::~PeopleCounter() {

}

void PeopleCounter::UpdateCounter(list<EnsembleTracker*> trackResult, Line vl,
		Size frameSize, cv::Mat& frame) {

	for (list<EnsembleTracker*>::iterator i = trackResult.begin();
			i != trackResult.end(); i++) {

		//(*i)->drawResult(frame, 1 / TRACKING_TO_BODYSIZE_RATIO);

		Rect win = (*i)->getResultHistory().back();
		Point center;
		center.x = scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).x
				+ scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).width / 2;
		center.y = scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).y
				+ scaleWin(win, 1 / TRACKING_TO_BODYSIZE_RATIO).height / 2;
		int testSide = vl.FindSide(vl.GetVP1().x, vl.GetVP1().y, vl.GetVP2().x,
				vl.GetVP2().y, center.x, center.y);

		int prevSide = (*i)->getObjSide();

//		if (testSide == 1)
//			cv::circle(frame, center, 3, cv::Scalar(0, 255, 0), 3);
//		else if (testSide == -1)
//			cv::circle(frame, center, 3, cv::Scalar(0, 0, 255), 3);
//		else if (testSide == 0)
//			cv::circle(frame, center, 3, cv::Scalar(255, 255, 255), 3);

		if ((prevSide == 1) && (testSide == -1)) {
			//object cross down the virtual line
			_out++;
			(*i)->setObjSide(testSide); //object side update
		} else if (prevSide == -1 && testSide == 1) {
			//object cross up the virtual line
			_in++;
			(*i)->setObjSide(testSide); //object side update
		} else if (testSide == 0) {
			//object on the line
			(*i)->setObjSide(testSide); //object side update
		} else if (prevSide == 0) {
			//set for the first time object side equal to actual side
			(*i)->setObjSide(testSide); //object side update
		}
	}
	_total = _in + _out;
}

int PeopleCounter::GetPeopleIn() {
	return _in;
}

int PeopleCounter::GetPeopleOut() {
	return _out;
}

int PeopleCounter::GetPeopleTotal() {
	return _total;
}
