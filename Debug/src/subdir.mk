################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/appTemplate.cpp \
../src/counter.cpp \
../src/dataReader.cpp \
../src/detector.cpp \
../src/main.cpp \
../src/multiTrackAssociation.cpp \
../src/munkres.cpp \
../src/tracker.cpp 

OBJS += \
./src/appTemplate.o \
./src/counter.o \
./src/dataReader.o \
./src/detector.o \
./src/main.o \
./src/multiTrackAssociation.o \
./src/munkres.o \
./src/tracker.o 

CPP_DEPS += \
./src/appTemplate.d \
./src/counter.d \
./src/dataReader.d \
./src/detector.d \
./src/main.d \
./src/multiTrackAssociation.d \
./src/munkres.d \
./src/tracker.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -I/usr/include/libxml2 -I/usr/include/libxml2/libxml -I/usr/local/include/opencv -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


